alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias ls="ls -F --color=auto"
alias sl='ls'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias less="less -R"
alias ack='ack-grep'
alias pyack='ack-grep --color --python'
alias cack='ack-grep --color --cc --cpp'
alias jack='ack-grep --color --java'
alias duhs='du -ks * | sort -n | cut -f2 | xargs -d "\n" du -sh'
alias title_case="rename 's/(^|[\s\(\)\[\]_-])([a-z])/\$1\u\$2/g' *"
alias dos2unix_recursive='find . -type f -exec dos2unix {} \;'
alias google="w3m www.google.com"
alias most_recent='ls -tr1 | tail -n 1'
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
alias +='pushd'
alias -- -='popd'
alias ?='dirs -v'
alias simulate_middle_click_paste='xsel | xvkbd -file -  2>/dev/null'
alias pp='source-highlight --out-format esc --output STDOUT -i $@'
alias devices_with_ip='ifconfig | grep "inet addr" -B 1 | grep "Link" | grep -v "lo" | cut -d " " -f 1'
alias diff='colordiff -y --width=200 $@ | less -R'

function togglecwd() 
{
    if [ "$OLDPWD" != "" ] && [ "$OLDPWD" != "$PWD" ]; then
        cd ~- 2>> /dev/null;
        pwd;
    fi
}
function _locate()
{
    locate $1 | grep $1
}
function findfile()
{
    find . -type f -print | grep "$1"
}
function x()
{
    if [ "$1" ] && [ -f "$1" ] ; then 
        case "$1" in
            *.tar)       tar xf "$1";;
            *.tar.bz2)   tar xjf "$1";;
            *.tar.gz)    tar xzf "$1";;
            *.tar.xz)    tar xJf "$1";;
            *.bz2)       bunzip2 "$1";;
            *.rar)       unrar e "$1";;
            *.gz)        gunzip "$1";;
            *.tar)       tar xf "$1";;
            *.tbz2)      tar xjf "$1";;
            *.tgz)       tar xzf "$1";;
            *.zip)       unzip "$1";;
            *.Z)         uncompress "$1";;
            *.7z)        7zr x "$1";;
            *)           echo "'$1' cannot be extracted via extract()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}
function view()
{
    if [ "$1" ] && [ -f "$1" ] ; then 
        case "$1" in
            *.pdf)      evince "$1" &;;
            *.chm)      kchmviewer "$1" &;;
            *.doc)      libreoffice "$1" &;;
            *.png)      eog "$1" &;;
            *.jpg)      eog "$1" &;;
            *.bmp)      eog "$1" &;;
            *.jpeg)     eog "$1" &;;
            *.html)     chromium-browser "$1" &;;
            *.htm)      chromium-browser "$1" &;;
            *.jpg)      eog -browser "$1" &;;
            *)          echo "'$1' cannot be viewed via read()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}
function play()
{
    SEARCH_TERM="$@"

    mpc clear
    mpc search artist "${SEARCH_TERM}" | mpc add
    mpc play
}


# Key Bindings
##################

bind '"\ef"':"\"grep '' ./ -RIn\C-[[D\C-[[D\C-[[D\C-[[D\C-[[D\C-[[D\C-[[D\C-[[D\C-[[D\""
bind '"\eF"':"\"grep '\\\\<\\\\>' ./ -RIn\C-[[D\C-[[D\C-[[D\C-[[D\C-[[D\C-[[D\C-[[D\C-[[D\C-[[D\C-[[D\C-[[D\""
bind '"\e6"':"\"togglecwd\C-m\""
bind '"XF86AudioPlay"':"\"banshee --pause\C-m\""
