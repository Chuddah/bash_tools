# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# Setup global bash environment

if [ -f ~/.bash_env ]; then
    source ~/.bash_env
fi

if [ -f ~/.bash_colours ]; then
    source ~/.bash_colours
fi

if [ -f ~/.bash_ps1 ]; then
    source ~/.bash_ps1
fi

# Setup utilities

if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi

if [ -f ~/.bash_work ]; then
    source ~/.bash_work
fi

# Display messages

if [ -f ~/.bash_motd ]; then
    source ~/.bash_motd
fi

# Inserted by Codescape MIPS SDK Pro to source SDK enviroment variables
if [ -r "/home/druscoe/.imgtec.sh" ]; then 
	source "/home/druscoe/.imgtec.sh"
fi
