" We use a vim
"
" Colo(u)red or not colo(u)red
" If you want color you should set this to true
"
syntax on
let color = "true"
"
if has("syntax")
    if color == "true"
	" This will switch colors ON
	so ${VIMRUNTIME}/syntax/syntax.vim
    else
	" this switches colors OFF
	syntax off
	set t_Co=0
    endif
endif

highlight DiffAdd term=reverse cterm=bold ctermbg=black ctermfg=green
highlight DiffChange term=reverse cterm=bold ctermbg=black ctermfg=blue
hi CursorLine   cterm=NONE ctermbg=darkred ctermfg=white guibg=darkred guifg=white
set viminfo='100,f1
set cursorline
set nocompatible
set bg=dark
set backspace=indent,eol,start
set tabstop=4
set softtabstop=4
set shiftwidth=4
set history=1000
set hidden
set showcmd
set number
set incsearch
set autoindent
set ruler
filetype plugin indent on
set hlsearch
set nowrap
set mouse=a
set expandtab
set smartcase
set pastetoggle=<F2>
set vb t_vb=""
set noswapfile



autocmd BufRead *.h set autoindent
autocmd BufRead *.h set noexpandtab
autocmd BufRead *.h set tabstop=4
autocmd BufRead *.h set shiftwidth=4
autocmd BufRead *.c set autoindent
autocmd BufRead *.c set noexpandtab
autocmd BufRead *.c set tabstop=4
autocmd BufRead *.c set shiftwidth=4
autocmd BufRead *.hpp set autoindent
autocmd BufRead *.hpp set noexpandtab
autocmd BufRead *.hpp set tabstop=4
autocmd BufRead *.hpp set shiftwidth=4
autocmd BufRead *.cpp set autoindent
autocmd BufRead *.cpp set noexpandtab
autocmd BufRead *.cpp set tabstop=4
autocmd BufRead *.cpp set shiftwidth=4
autocmd BufRead *.cxx set autoindent
autocmd BufRead *.cxx set noexpandtab
autocmd BufRead *.cxx set tabstop=4
autocmd BufRead *.cxx set shiftwidth=4
autocmd BufRead *.inl set autoindent
autocmd BufRead *.inl set noexpandtab
autocmd BufRead *.inl set tabstop=4
autocmd BufRead *.inl set shiftwidth=4
" autocmd CursorMoved * silent! exe printf('match Search /\V\<%s\>/', escape(expand('<cword>'), '/\'))


" execute "set <A-S-H>=\eH"
" execute "set <A-h>=\eh"
" execute "set <A-S-J>=\eJ"
" execute "set <A-j>=\ej"
" execute "set <A-S-K>=\eK"
" execute "set <A-k>=\ek"
" execute "set <A-S-L>=\eL"
" execute "set <A-l>=\el"
" execute "set <A-S-G>=\eG"
" execute "set <A-g>=\eg"
" execute "set <A-S-F>=\eF"
" execute "set <A-f>=\ef"


nnoremap ; :

" :nmap - Display normal mode maps
" :imap - Display insert mode maps
" :vmap - Display visual and select mode maps
" :smap - Display select mode maps
" :xmap - Display visual mode maps
" :cmap - Display command-line mode maps
" :omap - Display operator pending mode maps

" Treat long lines as break lines (useful when moving around in them)
map j gj
map k gk

" Keep the selection after indent in visual mode
vmap < <gv
vmap > >gv

" Toggle paste mode on and off
map <leader>pp :setlocal paste!<cr>

" Smart way to move between windows
nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l

nmap <A-j> mz:m+<cr>`z
nmap <A-k> mz:m-2<cr>`z
nmap <A-h> <<
nmap <A-l> >>
vmap <A-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <A-k> :m'<-2<cr>`>my`<mzgv`yo`z
vmap <A-h> <
vmap <A-l> >

autocmd BufRead *.py set makeprg=python\ -c\ \"import\ py_compile,sys;\ sys.stderr=sys.stdout;\ py_compile.compile(r'%')\"
autocmd BufRead *.py set efm=%C\ %.%#,%A\ \ File\ \"%f\"\\,\ line\ %l%.%#,%Z%[%^\ ]%\\@=%m
autocmd BufRead *.py nmap <F4> :!/usr/bin/python %<CR>
autocmd BufRead *.py nmap <F5> :!python %<CR>
autocmd BufRead *.py nmap <F6> :!LD_LIBRARY_PATH=/home/ldap/druscoe/gtkbuild/lib/:/home/ldap/druscoe/Source/libxcb-1.10/build/lib:$LD_LIBRARY_PATH gdb -tui -q python -ex "run %"<CR>

autocmd BufRead *.py imap :tb import traceback<cr>traceback.print_stack()<CR>
autocmd BufRead *.py imap :te import traceback<cr>traceback.print_exc()<CR>
autocmd BufRead *.py imap :ts :tb
autocmd BufRead *.py imap :test <CR>@test<cr>def test_FOO():<cr>expected = FISH<CR>result = something_to_test(input)<CR>assertEquals(expected, result)<CR><ESC>?FOO<CR>cw
autocmd BufRead *.py imap :ntest <CR>@test([<cr>(EXPECTED, INPUT),<cr>])<ESC><<odef test_FOO(expected, input):<cr>result = something_to_test(input)<CR>assertEquals(expected, result)<CR><ESC>?FOO<CR>cw
autocmd BufRead *.py nmap :tb i:tb
autocmd BufRead *.py nmap :ts i:ts
autocmd BufRead *.py nmap :te i:te
autocmd BufRead *.py nmap :test i:test
autocmd BufRead *.py nmap :ntest i:ntest
autocmd BufRead *.py nmap <A-f> "zyiw:!pyack "<C-r>z"<CR>
autocmd BufRead *.py nmap <A-S-f> "zyiw:!pyack "\b<C-r>z\b"<CR>

autocmd BufRead SConstruct nmap <F5> :!scons % verbose=1<CR>

autocmd BufRead *.cpp,*.c,*.h,*.hpp nmap <F5> :!g++ -std=gnu++0x % && ./a.out<CR>
autocmd BufRead *.cpp,*.c,*.h,*.hpp nmap <A-f> "zyiw:!grep "<C-r>z" -RIn ./ --color=always<CR>
autocmd BufRead *.cpp,*.c,*.h,*.hpp nmap <A-S-f> "zyiw:!grep "\b<C-r>z\b" -RIn ./ --color=always<CR>

command! CT set noexpandtab | set softtabstop=0 | set tabstop=4
command! GCC set expandtab | set softtabstop=2 | set tabstop=8


" Put plugins and dictionaries in this dir (also on Windows)
let vimDir = '$HOME/.vim'
let &runtimepath.=','.vimDir

" Keep undo history across sessions by storing it in a file
if has('persistent_undo')
    let myUndoDir = expand(vimDir . '/undodir')
    " Create dirs
    call system('mkdir ' . vimDir)
    call system('mkdir ' . myUndoDir)
    let &undodir = myUndoDir
    set undofile
endif

" ~/.vimrc ends here

